# Ambil Data API -> Kelola Data -> Visualisasi
# 1. Ambil Data
# 2. Konversikan Bytes > string > dictionary
# 3. Reshape Data
# 4. Visualisasi Data

# Import Library 
import requests
import json
import matplotlib.pyplot as plt
from datetime import datetime

# Ambil Data BTC dari Indodax
data_btc = requests.get('https://indodax.com/api/btc_idr/trades').content.decode('UTF-8')
data_btc = json.loads(data_btc)

# Kita Pisah Mana Transaksi Buy Dan Sell
buy = []
sell = []
for i in data_btc:
    if i['type'] == 'buy':
        buy.append(i)
    else:
        sell.append(i)

# Visualisasi Grafik Pembelian (BUY)
tanggal = []
harga = []
for i in buy:
    tanggal.append(datetime.utcfromtimestamp(int(i['date'])).strftime('%Y-%m-%d %H:%M:%S'))
    harga.append(i['price'])
    
plt.plot(tanggal, harga, marker="o", color="red")
plt.title('Harga BTC', fontsize=14)
plt.xlabel('Tanggal', fontsize=14)
plt.ylabel('Harga', fontsize=14)
plt.grid(True)
plt.show()
