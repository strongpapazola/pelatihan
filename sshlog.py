# log = '''Oct 23 09:05:00 mail sshd[6683]: Failed password for invalid user root from 61.177.173.16 port 35842 ssh2
# Oct 23 09:05:02 mail sshd[6683]: Failed password for invalid user root from 61.177.173.16 port 35842 ssh2
# Oct 23 09:05:04 mail sshd[6683]: Failed password for invalid user root from 61.177.173.16 port 35842 ssh2
# Oct 23 09:05:15 mail sshd[6686]: Failed password for invalid user centos from 103.41.205.184 port 53060 ssh2
# Oct 23 09:05:26 mail sshd[6689]: Failed password for invalid user root from 111.229.146.117 port 55328 ssh2
# Oct 23 09:05:29 mail sshd[6685]: Failed password for invalid user steam from 185.217.1.246 port 31443 ssh2
# Oct 23 09:05:32 mail sshd[6691]: Failed password for invalid user nexus from 103.41.205.184 port 53786 ssh2
# Oct 23 09:06:09 mail sshd[6695]: Failed password for invalid user root from 198.55.123.204 port 35090 ssh2
# Oct 23 09:06:09 mail sshd[6693]: Failed password for invalid user admin from 185.217.1.246 port 42534 ssh2
# '''

log = open('logfailedpassword.txt', 'r').read()

# Kelola Log nya dengan mengambil IP
list_ip = []
for i in log.splitlines():
    ip = i.split(' from ')[1]
    ip = ip.split(' port ')[0]
    if ip not in list_ip:
        list_ip.append(ip)

# Import Library (Paket)
import requests
import json
from tabulate import tabulate

# Ambil Informasi IP Setiap Penyerang
data_show = []
for i in list_ip:
    data = json.loads(requests.get(f'https://ipinfo.io/{i}').content.decode('UTF-8'))
    data_show.append([i, data['city'], data['region'], data['country']])

#Visualisasi
print(tabulate(data_show))

