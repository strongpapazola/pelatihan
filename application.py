import re
import pymongo

connection_url = "mongodb://localhost:27017/"

client = pymongo.MongoClient(connection_url)

# Melihat List Database Di MongoDB
# print(client.list_database_names()) 

# Mengakses DB Di MongoDB
db = client['pelatihan'] 

# Mengakses Collection
barang = db['barang']

# Melihat List Collection Di MongoDB
# print(db.list_collection_names())

# ============== Menghapus Data ==============
# query = {
#     "nama": "tas"
# }
# print(barang.delete_one(query))


# ============== Mengubah Data ==============
# query = {
#     "nama": "tas"
# }
# perubahan = {
#     "$set": {
#         "harga" : 100000
#     }
# }
# print(barang.update_one(query, perubahan))


# ============== Memasukan Data ==============
# data = {
#     "nama" : "pensil",
#     "harga": 3000
# }
# barang.insert_one(data)

# data = [
#     {
#         "nama" : "buku",
#         "harga": 5000
#     },
#     {
#         "nama" : "pulpen",
#         "harga": 2000
#     },
# ]
# barang.insert_many(data)

# ============== Mengambil Data ==============

# result = []
# for barisdata in barang.find():
#     result.append(barisdata)

# result = [x for x in barang.find()]
# print(result)

