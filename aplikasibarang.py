import pymongo
from tabulate import tabulate

connection_url = "mongodb://localhost:27017/"
client = pymongo.MongoClient(connection_url)
barang = client['pelatihan']['barang']

def ambildata():
    result = []
    for i in barang.find():
        result.append([i['nama'], i["harga"]])
    print("")
    print(tabulate(result, headers=["Nama", "Harga"]))

def masukandata():
    data = {}
    data['nama'] = input("Masukan Nama Barang : ")
    data['harga'] = input("Masukan Harga Barang : ")
    print(f"Barang Berhasil Dimasukan Dengan ID : {barang.insert_one(data).inserted_id}")

def ubahdata():
    query = {
        "nama" : input("Masukan Nama Barang : ")
    }
    data = {
        "$set": {
            "harga" : input("Masukan Harga Barang : ")
        }
    }
    barang.update_one(query, data)
    print(f"Barang Berhasil Diubah Dengan Nama : {query['nama']}")

def hapusdata():
    query = {
        "nama" : input("Masukan Nama Barang : ")
    }
    barang.delete_one(query)
    print(f"Barang Berhasil Dihapus Dengan Nama : {query['nama']}")

    
def menu():
    menu = """
    ============ Aplikasi Pengelolaan Barang ============

    1. Lihat Data
    2. Masukan Data
    3. Ubah Data
    4. Hapus Data
    """
    print(menu)

def pilih(argument):
    switcher = {
        1: ambildata,
        2: masukandata,
        3: ubahdata,
        4: hapusdata,
    }
    return switcher.get(argument, menu)
 
while True:
    try:
        menu()
        no = input("Masukan Angka : ")
        pilih(int(no))
    
        # if (pilih == "1"):
        #     ambildata()
        # elif (pilih == "2"):
        #     masukandata()
        # elif (pilih == "3"):
        #     ubahdata()
        # elif (pilih == "4"):
        #     hapusdata()
        # else:
        #     print("Masukan Salah")
    except Exception as e:
        print(f"Error : {e} ")